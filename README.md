Set of utility scripts for interacting with Neo4J docker container

# Create Neo4J docker container

```bash
docker pull neo4j
```


# Commands

- `neopull`: retrieve the neo4j container
- `neostart`: start the docker container in non-persistant mode(listen on ports 7474 and 7687)
- `neostartpersistent`: start the docker container in persistant mode (listen on ports 7474 and 7687)
- `neostop`: stop and remove the container (`docker rm -f [containerIdent]`)
- `neostatus`: give the status of docker containers
- `neoupdate`: update the docker container
- `neolog`: display the log of the neo4j container
- `neoclean`: **TODO** (`docker container prune`; not usefull anymore now that the `-f` option is used in neostop?)


# Todo

- [x] add installation script
